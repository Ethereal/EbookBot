# EbookBot
It's like an ebook. But not really.

## HOW 2 MAKE IT WORKS

1 - Create an app at dev.twitter.com, grab your api keys and your tokens (http://b.1339.cf/hejjjgp.png)

2 - Clone the repo (`git clone https://github.com/Ethernyan/EbookBot`)

2 - cd the project folder (`cd EbookBot`)

3 - Edit twitter_settings.py with your favorite text editor 
  - In screen_name, you should put the @username of your bot
  - In token, put your key and secret ("access token" and "access token secret")
  - In consumer, put your key and secret ("Consumer Key (API Key)" and "Consumer Secret (API Secret)")

4 - Create a crontab for your script. 
  - Type `crontab -e` (or `EDITOR=nano crontab -e` if you want to use nano, otherwise, the choice is yours)
  - Here is my crontab : 
  `*/20 * * * * cd /root/EbookBot && python2 main.py >> tweets.log`
  `* * * * * cd /root/EbookBot && python2 replies.py >> replies.log`
  - With this one, the bot will tweet every 20 minutes, and reply to mentions every minute.
